package be.optis.bootcamp;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "Orders")
public class Order {
    public Order() {
    }

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    public Long orderId;
    @ManyToMany
    @Cascade(CascadeType.ALL)
    @Size(min = 1, message = "At least 1 item is required")
    private List<OrderLine> orderLines;
    private boolean delivery;
    private double price;

    public Long getOrderId() {
        return orderId;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public boolean isDelivery() {
        return delivery;
    }

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
