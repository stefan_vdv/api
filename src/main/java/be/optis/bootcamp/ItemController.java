package be.optis.bootcamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/items")
public class ItemController {

    @Autowired
    private ItemRepository itemRepository;

    @PostConstruct
    private void initDB() {
        List<Item> itemList = new ArrayList<>() {
            {
                add(new Item("Mexicano", 5.0));
                add(new Item("Kaas", 3.5));
                add(new Item("Ham&Kaas", 4.0));
                add(new Item("Gezond", 4.5));
            }
        };
        itemRepository.saveAll(itemList);
    }

    @GetMapping
    public List<Item> findAll(Pageable pageable, @RequestParam(required = false) String name) {
        Page<Item> page = itemRepository.findAll(pageable);
        if(name == null){
            return page.getContent();
        }
        else{
            return page.stream()
                    .filter(e -> e.name.toLowerCase().contains(name.toLowerCase()))
                    .collect(Collectors.toList());
        }
    }

    @PostMapping
    public void create(@RequestBody Item item) {
        itemRepository.save(item);
    }

    @PutMapping("/{id}")
    public Item updateItem(@RequestBody Item updateItem, @PathVariable Long id) {
        Item item = itemRepository.findById(id).orElseThrow();
        item.price = updateItem.price;
        item.name = updateItem.name;
        item = itemRepository.save(item);
        return item;
    }

    @DeleteMapping("/{id}")
    public void deleteItem(@PathVariable Long id) {
        itemRepository.deleteById(id);
    }
}