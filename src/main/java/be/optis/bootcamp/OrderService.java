package be.optis.bootcamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public void addOrder(Order order){
        double priceItems = order.getOrderLines().stream().mapToDouble(OrderLine::getPrice).sum();
        if(order.isDelivery() && priceItems < 25){
            priceItems += 5;
        }
        order.setPrice(priceItems);
        orderRepository.save(order);
    }
}
