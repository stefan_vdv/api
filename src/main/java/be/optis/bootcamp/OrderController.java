package be.optis.bootcamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import java.util.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private OrderService orderService;

    @PostMapping
    public ResponseEntity<String> createOrder(@RequestBody List<Long> itemIds, @RequestParam boolean delivery){
        try{
            Iterable<Item> orderItems = itemRepository.findAllById(itemIds);
            List<OrderLine> orders = new ArrayList<>();
            orderItems.forEach(e -> {
                int count = Collections.frequency(itemIds, e.itemId);
                OrderLine orderLine = new OrderLine(e.name, e.price);
                if(count > 1){
                    for (int i = 0; i < count; i++) {
                        orders.add(orderLine);
                    }
                }
                else{
                    orders.add(orderLine);
                }
            });
            Order order = new Order();
            order.setOrderLines(orders);
            order.setDelivery(delivery);
            orderService.addOrder(order);
            return ResponseEntity.ok("Order created");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public Map<String, String> handleValidationException(ConstraintViolationException ex){
        Map<String, String> errors = new HashMap<>();
        ex.getConstraintViolations().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
