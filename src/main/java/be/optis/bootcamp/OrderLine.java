package be.optis.bootcamp;

import javax.persistence.*;

@Entity
public class OrderLine {
    public OrderLine() {

    }

    public OrderLine(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long orderLineId;
    private String name;
    private Double price;

    public Long getOrderLineId() {
        return orderLineId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
