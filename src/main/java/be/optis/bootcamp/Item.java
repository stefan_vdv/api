package be.optis.bootcamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Item {
    public Item(){}
    public Item(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    public Long itemId;
    public String name;
    public Double price;
}
